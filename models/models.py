# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Books(models.Model):
    _name = "books"  # name of the table in DB
    _description = "Books in The library"
# Columns                #what user see
    title = fields.Char(string="Title", required=False, )
    serial_number = fields.Integer(string="Serial Number", required=True, )
    author = fields.Char(string="Author", required=False, )
    publisher = fields.Char(string="Publisher", required=False, )
    edition = fields.Integer(string="Edition", required=False, )
    state = fields.Selection(string="Status", selection=[('A', 'Available'), ('UN', 'Unavailable'), ], required=False, )
    category = fields.Selection(string="Category", selection=[('CS', 'Computer Science'), ('PSY', 'Psychology'),
                                                              ('RE', 'Religion'), ('SS', 'Social Sciences'),
                                                              ('lang', 'Language'), ('SC', 'Sciences'),
                                                              ('literature', 'Literature'), ('art', 'Arts'),
                                                              ('history', 'History and Geography'), ], required=False, )


class Student(models.Model):
    _name = 'student_Info'
    _description = 'student'
    # columns
    id = fields.integer(string="ID", required=True, )
    name = fields.char(string="Name", required=False, )
    gender = fields.Selection(string="Gender", selection=[('male', 'Male'), ('female', 'Female'), ], required=False, )
    phone = fields.Integer(string="Phone", required=False, )
    email = fields.Char(string="Email", required=False, )


class Request(models.Model):
    _name = 'request_Info'
    _description = 'book request'
    # columns
    request_id = fields.Integer(string="Request ID", required=True, )
    student_id = fields.Many2one(comodel_name="student_Info", string="Student", required=False, )
    employees_id = fields.Many2one(comodel_name="hr.employee", string="Employees", required=False, )
    books_id = fields.Many2one(comodel_name="books", string="Books", required=False, )